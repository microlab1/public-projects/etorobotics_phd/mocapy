#!/usr/bin/env python3

import rospy

# Import Mocapy
import os, sys
mocapy_rootdir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.append(mocapy_rootdir)
from src.mocapy import *

# Import built-in packages
from queue import Queue
from threading import Thread
import time

# Import message
from mocapy.msg import Positions

# =============================== TIME FUNC =============================

def time_format(time, digits=0, hours_needed=False):
    """Returns a string of the *time* in the classic time format"""

    # Formatting
    if digits > 0:
        format_string = "%02i:%0" + str(digits+3) + "." + str(digits) + "f"
    else:
        format_string = "%02i:%02i"

    time = round(time, digits)
    seconds = time % 60

    if hours_needed:
        hours = int(time / 3600) % 24
        minutes = int(time / 60) % 60
        format_string = "%02i:" + format_string
        return format_string % (hours, minutes, seconds)
    else:
        minutes = int(time / 60)
        return format_string % (minutes, seconds)


# =============================== INIT ==============================

def mocapy_node():

    pos     = rospy.Publisher('mocapy_stream', Positions, queue_size=10)
    rate    = rospy.Rate(20)
    msg_pos = Positions()
    
    # --------------------------- RIGID BODIES TO TRACK ---------------------------
    # -------------- Feel free to update these lists for your project -------------
    
    # Create references of the sub-messages
    msg_pos_refs = [msg_pos.robot, msg_pos.owner, msg_pos.stranger, msg_pos.toy]
    # Use nametags to find rigid bodies
    nametags = ["ROB", "OWN", "STR", "TOY"]
    
    
    # Use test CSV
    use_csv = False
    
    # CSV log
    csv_log = True
    
    # -----------------------------------------------------------------------------
    # -----------------------------------------------------------------------------
    
    # Sanity check: len of the two lists must be equal
    num_of_rbs = len(nametags)
    if num_of_rbs != len(msg_pos_refs):
        rospy.loginfo('HEEEEYYYY!!!')

    # Init msg with zeros
    for pos_ref in msg_pos_refs:
        pos_ref.point_x       = 0
        pos_ref.point_y       = 0
        pos_ref.point_z       = 0
        pos_ref.orientation_x = 0
        pos_ref.orientation_y = 0
        pos_ref.orientation_z = 0
        pos_ref.tracked       = False
    
    # Init Mocapy
    queue = Queue()
    mocapy = MoCap("192.168.100.105", queue=queue, y_up=True)
    if use_csv:
        t_csv = Thread(target=mocapy.csv_read, args=(mocapy_rootdir + "/ros/test/ROB_test.csv",))
        t_csv.start()
    else:
        mocapy.natnet_start()

    # Mocapy init finished
    rospy.loginfo('Mocapy node initialized')

    counter = 0

    # ============================== CSV LOG =============================

    if csv_log:
        str_time = time_format(time.time(), hours_needed=True).replace(":", "-")

        current_dir = os.path.dirname(__file__)
        csv_path = os.path.join(current_dir, "measurements", "meas_" + str_time + ".csv")

        content = "Frame,Time,ROB.position.x,ROB.position.y,ROB.rot_Z"

        # Write file
        with open(csv_path, 'w+') as f:
            f.write(content)


# ============================== STREAM =============================

    while not rospy.is_shutdown():
        counter += 1

        # Checking for empty queue and shutdown
        while queue.empty() and not rospy.is_shutdown():
            time.sleep(0.05)  # In case of empty queue, wait
        
        # If is_shutdown, exit
        if rospy.is_shutdown():
            break

        # Popping a frame from queue
        frame = queue.get()
        
        # Get values
        for i in range(num_of_rbs):
            # Get rigid body
            rb = frame.rigid_body_get(nametags[i])
            if rb is None:
                rb = RigidBody(-1)
            
            # Copy rb data
            msg_pos_refs[i].point_x       = rb.position.x
            msg_pos_refs[i].point_y       = rb.position.y
            msg_pos_refs[i].point_z       = rb.position.z
            msg_pos_refs[i].orientation_x = 0
            msg_pos_refs[i].orientation_y = 0
            msg_pos_refs[i].orientation_z = rb.rotation.project("Z")
            msg_pos_refs[i].tracked       = rb.tracked
            
            # Log to CSV
            if csv_log and nametags[i] == "ROB":
                content = "\n" + str(frame.sn) + "," + str(frame.time) + "," + str(rb.position.x) + "," + str(rb.position.y) + "," + str(rb.rotation.project("Z"))

        
        with open(csv_path, 'a') as f:
            f.write(content)


        # Publish position info
        pos.publish(msg_pos)         # Publishing data
        #oth.publish(count)         # Publishing data on others' topic

        rospy.loginfo("Message %d sent"%counter)
        rate.sleep()
        
        
        
    if rospy.is_shutdown():
        rospy.loginfo("Stopping Mocapy")
        
        if use_csv:
            mocapy.csv_stop()
            t_csv.join()
        else:
            mocapy.natnet_stop()
            


if __name__ == "__main__":
    rospy.init_node("mocapy")
    mocapy_node()


