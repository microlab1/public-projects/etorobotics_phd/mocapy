# MOCAPY - Manual

_Implemented by Soma Deme - [demesoma@gmail.com](mailto:demesoma@gmail.com)_

Mocapy is a MoCap client written in Python. It can get data from Motive in two ways (reading a CSV file or receiving MoCap stream). It puts the data into its own data structure, then passes it on for further processing.


## Table of Contents
[[_TOC_]]


## Data structure

Mocapy works with its own data structure. You can see the hierarchic system that is used to store data in the figure below.

![](/doc/pics/data_struct.svg)
<br>***Figure:** Mocapy's data structure*<br>

- `Frame`: This is the highest level of the structure. This contains number and time of frame, entities and methods for operations like changing the used coordinate-system
- `RigidBody`: Contains data of a rigid body like name, position, rotation, if it's successfully tracked and time of last successful tracking
- `Marker`: Contains data of a marker, much like `RigidBody`, but it doesn't contain rotation.
- `Vector`: 3D vector for storing position data. If it contains XYZ Euler rotation, it can be converted to `Quaternion`.
- `Quaternion`: Quaternion for storing rotation data


## Coordinate-system

Mocapy uses Z-UP coordinates at all times, but is capable of receiving data with Z-UP and Y-UP coordinates as well. (Motive uses Y-UP coordinates as default.) Mocapy has a boolean variable called `y_up` with a default value of `True`. This decides whether it is necessary to rotate the coordinates in order to get Z-UP coordinate system. If your data source uses Z-UP coordinates, you have to set this to `False`. `Frame`, `RigidBody`, `Marker`, `Vector` and `Quaternion` have a method called `yup_to_zup()`, so you can convert coordinates manually. Note that while `Frame.yup_to_zup()` will convert coordinates for all entities, `Vector.yup_to_zup()` will convert data for that specific `Vector` only.


## Communication

### Reading CSV-files
Mocapy can read CSV-files (exported from Motive) using `Mocapy.csv_read()`. These files should have the following specifications:
- Rotation Type: Quaternion or XYZ (however Quaternion is preferred)

You can stop this process by using `Mocapy.csv_stop()`

### MoCap stream
You can receive data from Motive real-time, by calling `Mocapy.natnet_start()`. Mocapy will connect to Motive at `Mocapy.server_ip_address` and start getting data. If you would like to stop this process, use `Mocapy.natnet_stop()`<br>
_Note: In order to receive MoCap stream, your device should be on the same LAN network as the one with Motive running._

### Queue
Regardless of the source of data, Mocapy will put the frames into its `Queue` one-by-one so it can be processed later by another process. In order to do this, you need to set this `Mocapy.queue` when creating an instance of `Mocapy` (or at least before you start getting data).


## Setting up rigid bodies

### Marker placement
Rigid bodies should have markers arranged asymmetricly. Symmetrical arrangements can be detected 2 ways so Motive will have a hard time deciding the orientation properly.

![](/doc/pics/symmetry.svg)
<br>***Figure:** Improperly markerd rigid body: it is impossible to decide the orientation properly due to symmetry*<br>

### Orientation
When defining the rigid bodies, the front of them should point in positive Z direction in Y-UP coordinate system (or positive X considering a Z-UP coordinate system).<br>
_Note: You can redefine the orientation later on, but it is much easier to do it at this stage._

![](/doc/pics/rot_zero.svg)
<br>***Figure:** Recommended orientation while defining a rigid body*<br>


## Functions

For detailed descriptions of the functions of this class, please read [Mocapy - Functions](/doc/mocapy_functions.md).


## Problems and Solutions
- **Cannot receive MoCap stream**:
    - Check if Motive has received a connection request from Mocapy! ('NET' changed color from yellow to green)
        - If it has, your firewall might be blocking information coming from Motive. Turn off your firewall or make an exception for Python!
        - If it has not, check if your device uses the same LAN network as the one with Mocapy running!


## Source and references
[NatNet SDK](https://optitrack.com/downloads/developer-tools.html#natnet-sdk)
    