<img src="/doc/pics/GPK_BME_MOGI.png">

# Mocapy
_Implemented by Soma Deme - [demesoma@gmail.com](mailto:demesoma@gmail.com)_

Mocap client in Python

## Quick setup guide for Mocapy-ROS
*Note: At the moment doing these steps results in Mocapy-ROS streaming the contents of `../mocapy/ros/test/STR_test.csv`*

1 - Clone this repo to your catkin directiory

    $ cd ~/catkin_ws/src
    $ git clone https://gitlab.com/microlab1/private-projects/etorobotics/mocapy.git

2 - Run a `catkin_make` command so Mocapy-ROS becomes runable

    $ cd ~/catkin_ws
    $ catkin_make

3 - Now you can run the Mocapy-ROS node(s)!

    [Terminal 1] $ roscore
    [Terminal 2] $ rosrun mocapy stream.py

## Manual
For more information, read [Mocapy manual](/doc/Manual.md)

## Known bugs
**Mocapy:**
- When reading CSVs with Euler angles, conversion to Quaternion is not done properly <br> **Use quaternions when exporting CSV from MoCap!**

