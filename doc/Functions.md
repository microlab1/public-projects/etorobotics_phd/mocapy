# MOCAPY - Functions

_Implemented by Soma Deme - [demesoma@gmail.com](mailto:demesoma@gmail.com)_

Here you can read most of the functions of the Mocapy class. The basic functions should be enough to use this class. Please, read [Mocapy - Manual](/doc/mocapy_manual.md) for more information on getting started.

## Table of Contents
[[_TOC_]]

## Basic functions

### `natnet_start()`
Connects to Motive and starts listening to it

-------
### `natnet_stop()`
Stops the streaming-client process

-------
### `server_address_set(server_ip_address)`
Sets the IP address of the server (Motive)
- `server_ip_address`: (string) The IP address you would like to set as server IP address

-------
### `csv_read(queue, file_path, queue_max=10, every_xth_line=1)`
Opens the CSV file at `file_path` and puts its contents into Frames, then the Frames are pushed into `self.queue`
- `file_path`: (string) Path of the CSV file you would like to read
- `every_xth_line`: (int) If this value is X, `csv_read()` will skip X-1 rows of data after each line read.

-------
### `csv_stop()`
Stops the CSV-read process

-------

