# encoding: UTF-8

from .NatNetClient import NatNetClient
from queue import Queue
from math import sqrt, pi, sin, cos, asin, atan2
import numpy as np
from copy import deepcopy
import socket
import time

# Not crucial imports
# It is possible to use Mocapy without some packages but with limited capabilities
# Tkinter
try:
    from tkinter import *
except:
    print("Warning: Unable to import tkinter --> GUI cannot be used!")
# Scipy
try:
    from scipy.spatial.transform import Rotation as R
except:
    print("Warning: Unable to import Scipy --> Quaternion-to-Euler and Euler-to-Quaternion conversions cannot be made!")



# Define a color set
rigid_body_color_palette = ['#CC1111', '#11CC11', '#1111FF', '#AAAA00', '#00AAAA', '#AA00AA',
                            '#AA5500', '#AA0055', '#66AA00', '#6600AA', '#00AA55', '#0066AA']
grey = '#424242'


# ======================================== MOCAP ========================================

class MoCap:
    def __init__(self, server_ip_address="192.168.100.105", local_ip_address=None, queue=Queue(), canvas=None, y_up=True):
        # Frame
        self.frame = Frame()

        # Canvas
        self.canvas = canvas

        # Queue
        self.queue = queue

        # Input coordinate type
        self.y_up = y_up

        # Reading CSV (terminate flag)
        self.reading = False

        if local_ip_address is None:
            # WARNING! AUTOMATIC IP DETECTION BASED ON A DUMMY RULE, TAKE CARE!
            myip = socket.gethostbyname(socket.gethostname())
            for IP in [i[4][0] for i in socket.getaddrinfo(socket.gethostname(), None)]:
                #BUGS: Az �j IP 192.168.100... alap�, erre ki k�ne tal�lni valamit. (Els� 3 tag szerverrel megegyez�, am�g a maszk 255.255.255.0)
                if IP.find("192.168.") >= 0:
                    myip = IP
        else:
            myip = local_ip_address

        print("Detected local IP of this PC: ", myip)
        # NatNetClient
        self.streamingClient = NatNetClient(server_ip_address=server_ip_address, local_ip_address=myip)

        self.streamingClient.new_frame_listener = self.__natnet_frame_rec
        self.streamingClient.rigid_body_listener = self.__natnet_rbody_rec
        self.streamingClient.rigid_body_description_listener = self.__natnet_rbody_desc_rec
        self.streamingClient.marker_listener = self.__natnet_marker_rec


    def __natnet_frame_rec(self, frameNumber, markerSetCount, unlabeledMarkersCount, rigidBodyCount, skeletonCount,
                           labeledMarkerCount, timecode, timecodeSub, timestamp, isRecording, trackedModelsChanged):
        """This is a callback function that gets connected to the NatNet client.
        It is called once per frame."""

        #print("Received frame", frameNumber)
        self.frame.sn = frameNumber
        
        # Get frame time and update last_seen for tracked entities
        self.frame.time = time.time()
        self.frame.last_seen_update()

        # Clear the queue
        while not self.queue.empty():
            self.queue.get()

        # Put the frame into the empty queue
        self.queue.put(deepcopy(self.frame))


    def __natnet_rbody_rec(self, frame_number, timestamp, id, position, rotation):
        """This is a callback function that gets connected to the NatNet client.
        It is called once per rigid body per frame."""

        rb = self.frame.rigid_body_get_by_id(id)
        if not rb:
            self.frame.rigid_body_add(RigidBody(id))
            rb = self.frame.rigid_body_get_by_id(id)

        pos = Vector(position[0], position[1], position[2])
        rot = Quaternion(rotation[3], rotation[0], rotation[1], rotation[2])

        # If needed, convert Y-up to Z-up
        if self.y_up:
            pos.yup_to_zup()
            rot.yup_to_zup()

        # If the position is the same set tracked to False
        if rb.position == pos and rb.rotation == rot:
            rb.tracked = False

        else:  # Else, update current position
            # Get position
            rb.position = pos

            # Get rotation
            rb.rotation = rot

            # Tracked
            rb.tracked = True
            rb.last_seen = self.frame.time


    def __natnet_rbody_desc_rec(self, id, name):
        """This is a callback function that gets connected to the NatNet client.
        It is called when NatNet client receives a rigid body description."""

        print("Received description for rigid body", name)
        self.frame.rigid_body_add(RigidBody(id, str(name)[2:-1]))


    def __natnet_marker_rec(self, id, position):
        """This is a callback function that gets connected to the NatNet client.
        It is called once per marker per frame."""

        m = self.frame.marker_get_by_id(id)
        if not m:
            self.frame.marker_add(Marker(id))
            m = self.frame.marker_get_by_id(id)
        m.position.x = position[0]
        m.position.y = position[1]
        m.position.z = position[2]

        if self.y_up:
            m.yup_to_zup()


    def csv_read(self, file_path, queue_max=10, every_xth_line=1):
        """Opens the CSV file at `file_path` and puts its contents into Frames, then the Frames are pushed into `self.queue`"""

        # ================================ INIT ================================

        # Reading
        self.reading = True

        # Clear frame
        self.frame.clear()

        # Import CSV
        with open(file_path, 'r') as f:

            # Reading metadata
            line = f.readline()
            meta = line.split(',')
            # Remove "\n" from the last string
            meta[len(meta) - 1].rstrip()

            # Search for Rotation Type
            quaternion = True
            for i in range(len(meta)):
                if meta[i] == "Rotation Type":
                    if meta[i+1] == "Quaternion":
                        quaternion = True
                        break
                    else:
                        quaternion = False
                        rot_type = meta[i+1].lower()
                        break

            # Reading empty lines
            for i in range(10):
                line = f.readline()
                if line != "\n":
                    break

            # ================================ HEADER ================================

            # Read entity types
            entity_types = line.split(',')
            # Remove "\n" from the last string
            entity_types[len(entity_types)-1].rstrip()

            # Read names
            line = f.readline().replace("\"", "")
            names = line.split(',')
            # Remove "\n" from the last string
            names[len(names) - 1].rstrip()

            # Read IDs
            line = f.readline().replace("\"", "")
            ids = line.split(',')
            # Remove "\n" from the last string
            ids[len(ids) - 1].rstrip()

            # Read data types
            line = f.readline()
            data_types = line.split(',')
            line = f.readline()
            tmp = line.split(',')
            # Remove "\n" from the last strings
            data_types[len(data_types) - 1].rstrip()
            tmp[len(tmp) - 1].rstrip()
            # Merge
            for i in range(len(data_types)):
                data_types[i] += tmp[i]

            # Add entities to frame and delete unnecessary columns
            cols_del = []
            for i in range(len(entity_types)):
                # Rigid body
                if entity_types[i] == "Rigid Body":
                    self.frame.rigid_body_add(RigidBody(ids[i], names[i]))

                # Marker
                if entity_types[i] == "Marker":
                    if "Unlabeled" not in names[i]:
                        self.frame.marker_add(Marker(ids[i], names[i]))
                    else:       # Unlabeled markers have no useful information
                        cols_del.append(i)

                # NOTE: If you need other entity types to be added, write code HERE

            """# Delete selected columns from dataframe and entity types
            df.drop(df.columns[cols_del], axis=1, inplace=True)
            for i in sorted(cols_del, reverse=True):
                del entity_types[i]"""

            # Assign markers to rigid bodies
            for m in self.frame.markers:
                dots_at = m.name.find(":")
                if dots_at > 0:
                    self.frame.rigid_body_get(m.name[0:dots_at]).markers.append(m)

            # ================================ DATA ================================

            print("Reading CSV")
            while True:
                for j in range(every_xth_line):
                    line = f.readline()

                # If end_of_file or csv_stop, break
                if line == "" or not self.reading:
                    break
                row = line.split(',')
                # Remove "\n" from the last cell
                row[len(row) - 1].rstrip()

                # Read frame number and time
                self.frame.sn = int(row[0])
                self.frame.time = float(row[1])

                # Init indexes
                for col in range(2, len(entity_types)):

                    # Updating current rigid body's coordinates
                    if entity_types[col] == "Rigid Body":
                        rb = self.frame.rigid_body_get_by_id(ids[col])

                        if row[col] == "":          # If MoCap could not get the coordinates
                            rb.tracked = False

                        # Position
                        elif data_types[col] == "PositionX":
                            rb.position.x = float(row[col])
                            rb.position.y = float(row[col + 1])
                            rb.position.z = float(row[col + 2])

                            rb.tracked = True
                            rb.last_seen = self.frame.time

                        # Rotation
                        elif data_types[col] == "RotationX":
                            rot_x = float(row[col])
                            rot_y = float(row[col + 1])
                            rot_z = float(row[col + 2])

                            # If rotation is in quaternion
                            if quaternion:
                                rot_w = float(row[col + 3])

                                rb.rotation = Quaternion(rot_w, rot_x, rot_y, rot_z)

                            else:
                                rb.rotation = Vector(rot_x, rot_y, rot_z).to_quaternion(rot_type, degrees=True)

                    # Updating current marker's coordinates
                    if entity_types[col] == "Marker" and "Unlabeled" not in names[col]:
                        m = self.frame.marker_get_by_id(ids[col])

                        if row[col] == "":     # Test if MoCap could not get the coordinates
                            m.tracked = False

                        # Position
                        elif data_types[col] == "PositionX":
                            m.position.x = float(row[col])
                            m.position.y = float(row[col+1])
                            m.position.z = float(row[col+2])

                            m.tracked = True


                    # NOTE: If you need other entity types to be added, write code HERE

                while self.queue.qsize() >= queue_max:
                    time.sleep(0.05)

                # If the input coordinates are in Y UP, convert them to Z UP
                if self.y_up:
                    self.frame.yup_to_zup(tracked_only=True)

                # Update last_seen for tracked entities
                self.frame.last_seen_update()

                self.queue.put(deepcopy(self.frame))

            print("CSV read finished")


    def csv_stop(self):
        """Stops the CSV-read process"""

        self.reading = False
        with self.queue.mutex:
                self.queue.queue.clear()


    def server_address_set(self, server_ip_address):
        """Sets the IP address of the server (Motive)"""

        self.streamingClient.server_ip_address = server_ip_address


    def server_address_get(self):
        """Returns the IP address of the server (Motive)"""

        return self.streamingClient.server_ip_address


    def natnet_start(self):
        """Connects to Motive and starts listening to it"""

        self.frame.clear()
        self.streamingClient.run()


    def natnet_stop(self):
        """Stops the streaming-client process"""

        self.streamingClient.stopit()

# ======================================== FRAME ========================================

class Frame:
    def __init__(self, sn=0, time=0, rigid_bodies=[], markers=[]):
        self.sn = sn
        self.time = time
        self.rigid_bodies = deepcopy(rigid_bodies)
        self.markers = deepcopy(markers)


    def __deepcopy__(self, memo):  # memo is a dict of id's to copies
        """Deepcopy of frame class. Using this copying method the instances will be independent from each other"""
        id_self = id(self)  # memoization avoids unnecessary recursion
        _copy = memo.get(id_self)
        if _copy is None:
            _copy = type(self)(
                deepcopy(self.sn, memo),
                deepcopy(self.time, memo),
                deepcopy(self.rigid_bodies, memo),
                deepcopy(self.markers, memo)
            )
            memo[id_self] = _copy
        return _copy


    def rigid_body_add(self, rigid_body):
        """Adds the given rigid body to the frame's list.
        If a rigid body with the given id already exists, it updates its name"""

        # If the ID of the rigid body exists, update the name of the existing rigid body
        for rb in self.rigid_bodies:
            if rb.id == rigid_body.id:
                rb.name = rigid_body.name
                return

        # If the ID doesn't exist, add the rigid body and give it a color if it doesn't have one
        if rigid_body.color is None:
            rigid_body.color = rigid_body_color_palette[len(self.rigid_bodies) % len(rigid_body_color_palette)]
        self.rigid_bodies.append(rigid_body)


    def rigid_body_update(self, rigid_body):
        """Same as rigid_body_add() :)"""
        self.rigid_body_add(rigid_body)


    def rigid_body_get(self, name):
        """Returns the rigid body named *name*"""
        for rb in self.rigid_bodies:
            if rb.name == name:
                return rb
        return None


    def rigid_body_get_by_id(self, id):
        """Returns the rigid body with id: *id*"""
        for rb in self.rigid_bodies:
            if rb.id == id:
                return rb
        return None


    def rigid_body_get_index(self, name):
        """Returns the index of the rigid body named *name*"""
        for i in range(len(self.rigid_bodies)):
            if self.rigid_bodies[i].name == name:
                return i
        return -1


    def rigid_body_get_index_by_id(self, id):
        """Returns the index of the rigid body with id: *id*"""
        for i in range(len(self.rigid_bodies)):
            if self.rigid_bodies[i].id == id:
                return i
        return -1


    def rigid_body_remove(self, name):
        """Removes frame's rigid body named *name*"""
        for rb in self.rigid_bodies:
            if name == rb.name:
                self.rigid_bodies.remove(rb)
                return True
        return False


    def marker_add(self, marker):
        """Adds the given marker to the frame's list.
        If a marker with the given id already exists, it updates its name"""
        for m in self.markers:
            if m.id == marker.id:
                m.name = marker.name
                return
        self.markers.append(marker)


    def marker_update(self, marker):
        """Same as marker_add() :)"""
        self.marker_add(marker)


    def marker_get(self, name):
        """Returns the marker named *name*"""
        for m in self.markers:
            if m.name == name:
                return m
        return None


    def marker_get_by_id(self, id):
        """Returns the marker with id: *id*"""
        for m in self.markers:
            if m.id == id:
                return m
        return None


    def marker_get_index(self, name):
        """Returns the index of the marker named *name*"""
        for i in range(len(self.markers)):
            if self.markers[i].name == name:
                return i
        return -1


    def marker_get_index_by_id(self, id):
        """Returns the index of the marker with id: *id*"""
        for i in range(len(self.markers)):
            if self.markers[i].id == id:
                return i
        return -1


    def marker_remove(self, name):
        """Removes frame's marker named *name*"""
        for m in self.markers:
            if name == m.name:
                self.markers.remove(m)
                return True
        return False


    def yup_to_zup(self, tracked_only=False):
        """Converts coordinates from Y UP to Z UP"""
        for rb in self.rigid_bodies:
            if rb.tracked or not tracked_only:
                rb.yup_to_zup()

        for m in self.markers:
            if m.tracked or not tracked_only:
                m.yup_to_zup()


    def last_seen_update(self):
        """Updates "last_seen" variables for entities that have been tracked"""
        for rb in self.rigid_bodies:
            if rb.tracked:
                rb.last_seen = self.time

        for m in self.markers:
            if m.tracked:
                m.last_seen = self.time


    def draw(self, canvas, scene_width=5, scene_height=5, origin_offset_x=0, origin_offset_y=0, r_markers=1, draw_markers=False, frame_check=None):
        """Draws rigid bodies (an markers) of the frame on a tkinter *canvas*"""

        # If canvas is None, return
        if not canvas:
            print("No canvas to draw on!")
            return

        # Calculate coordinate conversions (multiplier, offsets)
        canvas_width = canvas.winfo_width()
        canvas_height = canvas.winfo_height()

        # Multiplier (m to pixel)
        if canvas_width/canvas_height > scene_width/scene_height:
            conv_mult = 0.95 * canvas_height / scene_height
        else:
            conv_mult = 0.95 * canvas_width / scene_width

        # room_offset (room's left bottom coordinate [pixel])
        room_offset_x = canvas_width/2 - scene_width*conv_mult/2
        room_offset_y = canvas_height/2 + scene_height*conv_mult/2

        # Draw room
        if canvas.find_withtag("room"):  # If the item already exists
            canvas.coords("room", room_offset_x, room_offset_y,
                          canvas_width - room_offset_x, canvas_height - room_offset_y)

        else:  # Else, create a new one
            canvas.create_rectangle(room_offset_x, room_offset_y,
                                    canvas_width - room_offset_x, canvas_height - room_offset_y,
                                    outline="white", tags="room")

        # Draw origin
        origin_x_px = room_offset_x + origin_offset_x * conv_mult
        origin_y_px = room_offset_y - origin_offset_y * conv_mult
        size = 30
        t_offs = 5
        col = "grey"

        if canvas.find_withtag("origin_a_x"):  # If the items already exist
            canvas.coords("origin_a_x", origin_x_px, origin_y_px, origin_x_px + size, origin_y_px)
            canvas.coords("origin_t_x", origin_x_px + size, origin_y_px - t_offs + 4)
            canvas.coords("origin_a_y", origin_x_px, origin_y_px, origin_x_px, origin_y_px - size)
            canvas.coords("origin_t_y", origin_x_px + t_offs, origin_y_px - size - 5)

        else:  # Else, create new ones
            canvas.create_line(origin_x_px, origin_y_px, origin_x_px + size, origin_y_px,
                               fill=col, tags="origin_a_x")
            canvas.create_text(origin_x_px + size, origin_y_px - t_offs + 4,
                               fill=col, tags="origin_t_x", text="x", anchor=SE)
            canvas.create_line(origin_x_px, origin_y_px, origin_x_px, origin_y_px - size,
                               fill=col, tags="origin_a_y")
            canvas.create_text(origin_x_px + t_offs, origin_y_px - size - 5,
                               fill=col, tags="origin_t_y", text="y", anchor=NW)

        # Get checkboxes from frame
        if frame_check is not None:
            checkboxes = frame_check.winfo_children()

        num_rigid_bodies = len(self.rigid_bodies)
        for i in range(num_rigid_bodies):
            # Choose a color based on index. If rigid body is not tracked, choose grey
            if self.rigid_bodies[i].tracked:
                col = self.rigid_bodies[i].color
            else:
                col = grey

            # Calculate the coordinates in canvas
            rot = self.rigid_bodies[i].rotation.project("Z")
            x = (self.rigid_bodies[i].position.x + origin_offset_x) * conv_mult + room_offset_x
            y = - (self.rigid_bodies[i].position.y + origin_offset_y) * conv_mult + room_offset_y
            r = self.rigid_bodies[i].r * conv_mult
            name = self.rigid_bodies[i].name

            # Tags for canvas items
            tag_circ = str(self.rigid_bodies[i].id) + "_circ"
            tag_line = str(self.rigid_bodies[i].id) + "_line"
            tag_text = str(self.rigid_bodies[i].id) + "_text"

            # If the items already exist
            if canvas.find_withtag(tag_circ):
                canvas.coords(tag_circ, x-r, y-r, x+r, y+r)
                canvas.coords(tag_line, x, y, x + r*cos(rot*pi/180), y - r*sin(rot*pi/180))
                canvas.coords(tag_text, x, y + r + 5)
                canvas.itemconfig(tag_circ, outline=col)
                canvas.itemconfig(tag_line, fill=col)
                canvas.itemconfig(tag_text, fill=col, text=self.rigid_bodies[i].name)
                if frame_check is not None:
                    checkboxes[i].configure(text=name, fg=col, activeforeground=col)
                    if checkboxes[i].variable.get() and self.rigid_bodies[i].last_seen is not None:
                        canvas.itemconfig(tag_circ, state='normal')
                        canvas.itemconfig(tag_line, state='normal')
                        canvas.itemconfig(tag_text, state='normal')
                    else:
                        canvas.itemconfig(tag_circ, state='hidden')
                        canvas.itemconfig(tag_line, state='hidden')
                        canvas.itemconfig(tag_text, state='hidden')

            # Else, create new ones
            else:
                canvas.create_oval(x-r, y-r, x+r, y+r, outline=col, width=3, tags=tag_circ)
                canvas.create_line(x, y, x + r*cos(rot*pi/180), y - r*sin(rot*pi/180), fill=col, width=3, tags=tag_line)
                canvas.create_text(x, y + 1.5 * r, text=name, fill=col, tags=tag_text, anchor=N)
                # If there's a check frame, create check boxes
                if frame_check is not None:
                    check = Checkbutton(frame_check, text=name, fg=col, bg="grey20", bd=0, activeforeground=col, activebackground="grey20")
                    check.variable = IntVar(value=1)
                    check.configure(variable=check.variable)
                    check.pack(anchor=W)



        if draw_markers:
            num_markers = len(self.markers)
            for i in range(num_markers):
                # Position
                x = (self.markers[i].position.x + origin_offset_x) * conv_mult + room_offset_x
                y = - (self.markers[i].position.y + origin_offset_y) * conv_mult + room_offset_y

                # Tag
                tag_mark = str(self.markers[i].id) + "_mark"

                if canvas.find_withtag(tag_mark):       # If the item already exists
                    canvas.coords(tag_mark, x-r_markers, y-r_markers, x+r_markers, y+r_markers)

                else:                                   # Else, create a new one
                    canvas.create_oval(x-r_markers, y-r_markers, x+r_markers, y+r_markers, outline='#FFF', fill='#FFF', tags=tag_mark)


    def clear(self):
        self.rigid_bodies = []
        self.markers = []
        self.time = 0
        self.sn = 0

# ======================================== VECTOR ========================================

class Vector:

    def __init__(self, x=0.0, y=0.0, z=0.0):
        """Initialize 3D Coordinates of the Vector"""
        self.x = x
        self.y = y
        self.z = z


    def magnitude(self):
        """Method to calculate magnitude of a Vector"""
        return sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)


    def angle(self):
        """Returns the 3D angle of a Vector in degrees"""
        return Vector(atan2(self.z, self.y) * 180 / pi,
                      atan2(self.x, self.z) * 180 / pi,
                      atan2(self.y, self.x) * 180 / pi)


    def to_quaternion(self, mode="xyz", degrees=False):
        """Returns the Quaternion equivalent of the euler Vector (Degrees!)"""

        q = R.from_euler(mode, [self.x, self.y, self.z], degrees=degrees).as_quat()
        return Quaternion(q[3], q[0], q[1], q[2])


    def yup_to_zup(self):
        tmp = self.z
        self.z = self.y
        self.y = self.x
        self.x = tmp


    def project(self, plane="Z"):
        """Projects 3D rotation to a plane returning a scalar rotation. (Degrees!)"""

        # Select plane, init vector to rotate
        plane.capitalize()
        if plane == "X" or plane == "YZ":
            mode = 0
            I = np.array([0, 1, 0]).reshape(3, 1)
        elif plane == "Y" or plane == "ZX":
            mode = 1
            I = np.array([0, 0, 1]).reshape(3, 1)
        elif plane == "Z" or plane == "XY":
            mode = 2
            I = np.array([1, 0, 0]).reshape(3, 1)
        else:
            return None

        # Convert rotations for the trigonometric functions
        x_rad = self.x * pi / 180
        y_rad = self.y * pi / 180
        z_rad = self.z * pi / 180

        # Rotation matrices
        rot_x = np.array([1,          0,           0,
                          0, cos(x_rad), -sin(x_rad),
                          0, sin(x_rad), cos(x_rad)]).reshape(3, 3)
        rot_y = np.array([cos(y_rad),  0, sin(y_rad),
                          0,           1,          0,
                          -sin(y_rad), 0, cos(y_rad)]).reshape(3, 3)
        rot_z = np.array([cos(z_rad), -sin(z_rad), 0,
                          sin(z_rad),  cos(z_rad), 0,
                          0,           0,          1]).reshape(3, 3)

        # Result of rotation (NOTE: The order of multiplications does matter!)
        res = rot_z.dot(rot_y.dot(rot_x.dot(I)))

        # Calculate projected rotation
        if mode == 0:
            rot = atan2(res[2], res[1]) * 180 / pi
        elif mode == 1:
            rot = atan2(res[0], res[2]) * 180 / pi
        else:
            rot = atan2(res[1], res[0]) * 180 / pi

        #print(round(float(res[0]), 3), "\t\t", round(float(res[1]), 3))
        return rot


    def __add__(self, V):
        """Method to add to Vector"""
        return Vector(self.x + V.x, self.y + V.y, self.z + V.z)


    def __sub__(self, V):
        """Method to subtract 2 Vectors"""
        return Vector(self.x - V.x, self.y - V.y, self.z - V.z)


    def __xor__(self, V):
        """Method to calculate the dot product of two Vectors"""
        return self.x * V.x + self.y * V.y + self.z * V.z


    def __mul__(self, V):
        """Method to calculate the cross product of 2 Vectors"""
        return Vector(self.y * V.z - self.z * V.y,
                      self.z * V.x - self.x * V.z,
                      self.x * V.y - self.y * V.x)


    def __eq__(self, V):
        if self.x == V.x and self.y == V.y and self.z == V.z:
            return True
        else:
            return False


    def __repr__(self):
        """Method to define the representation of the Vector"""
        return "( " + str(self.x) + " ; " + str(self.y) + " ; " + str(self.z) + " )"


    def from_str(self, str):
        """Copies the content of a string to the Vector itself"""

        # Strip the parantheses from the string and split it
        str = str[2:-2]
        vec = str.split(";")

        # Copy the contents to the vector
        self.x = float(vec[0]);    self.y = float(vec[1]);    self.z = float(vec[2]);

# ======================================== QUATERNION ========================================

class Quaternion:
    def __init__(self, w=0.0, x=0.0, y=0.0, z=0.0):
        self.w = w
        self.x = x
        self.y = y
        self.z = z


    def __mul__(self, q):
        """Method to calculate the product of 2 Quaternions"""
        return Quaternion(-self.x * q.x - self.y * q.y - self.z * q.z + self.w * q.w,
                          self.x * q.w + self.y * q.z - self.z * q.y + self.w * q.x,
                          -self.x * q.z + self.y * q.w + self.z * q.x + self.w * q.y,
                          self.x * q.y - self.y * q.x + self.z * q.w + self.w * q.z)


    def __invert__(self):
        return Quaternion(self.w, -self.x, -self.y, -self.z)


    def yup_to_zup(self):
        tmp = self.z
        self.z = self.y
        self.y = self.x
        self.x = tmp


    def project(self, plane="Z"):
        """Projects Quaternion rotation to a plane returning a scalar rotation. (Degrees!)"""

        # Select plane, init vector to rotate
        plane.capitalize()
        if plane == "X" or plane == "YZ":
            mode = 0
            I = Quaternion(0, 0, 1, 0)
        elif plane == "Y" or plane == "ZX":
            mode = 1
            I = Quaternion(0, 0, 0, 1)
        elif plane == "Z" or plane == "XY":
            mode = 2
            I = Quaternion(0, 1, 0, 0)
        else:
            return None

        # Result of rotation
        res = self * I * (~self)

        # Calculate projected rotation
        if mode == 0:
            rot = atan2(res.z, res.y) * 180 / pi
        elif mode == 1:
            rot = atan2(res.x, res.z) * 180 / pi
        else:
            rot = atan2(res.y, res.x) * 180 / pi

        return rot


    def to_vector(self, mode="xyz", degrees=False):
        """Returns the euler Vector equivalent of the Quaternion"""

        euler = R.from_quat([self.w, self.x, self.y, self.z]).as_euler(mode, degrees=degrees)
        return Vector(euler[0], euler[1], euler[2])


    def __eq__(self, Q):
        if self.x == Q.x and self.y == Q.y and self.z == Q.z and self.w == Q.w:
            return True
        else:
            return False


    def __repr__(self):
        """Method to define the representation of the Vector"""
        return str(self.w) + " + " + str(self.x) + "i + " + str(self.y) + "j + " + str(self.z) + "k"

# ======================================== RIGID BODY ========================================

class RigidBody:
    def __init__(self, id, name="Unnamed", position=Vector(), rotation=Quaternion(), r=0.2, color=None, tracked=True, last_seen=None, markers=[]):
        
        self.id = id
        self.name = name
        self.position = deepcopy(position)
        self.rotation = deepcopy(rotation)
        self.r = r
        self.color = color
        self.tracked = tracked
        self.last_seen = last_seen
        self.markers = deepcopy(markers)


    def set_r(self, r):
        """Sets the radius of the RigidBody"""

        self.r = r


    def yup_to_zup(self):
        """Converts coordinates from Y UP to Z UP"""
        self.position.yup_to_zup()
        self.rotation.yup_to_zup()

# ======================================== MARKER ========================================

class Marker:
    def __init__(self, id, name="Unnamed", position=Vector(), tracked=True, last_seen=0):
        self.id = id
        self.name = name
        self.position = deepcopy(position)
        self.tracked = tracked
        self.last_seen = last_seen


    def yup_to_zup(self):
        """Converts coordinates from Y UP to Z UP"""
        self.position.yup_to_zup()



if __name__=='__main__':
    mc = MoCap("192.168.100.105")
    mc.natnet_start()

    # DEBUG DELETE START
    for i in range(-200, 200):
        # Quaternion that rotates around (a; b; c) axis
        a = 0
        b = 0
        c = sqrt(1 - a**2 - b**2)
        q = Quaternion(cos(i*pi/180/2), sin(i*pi/180/2)*a, sin(i*pi/180/2)*b, sin(i*pi/180/2)*c)

        # Convert that to euler
        v = q.to_vector(degrees=True)

        # Print results from both methods
        print("i:", i, "\teuler:", round(v.project("Z")), "\tquat:", round(q.project("Z")))

        # Sleeping is very important!
        # You should sleep enough too if you want to function properly the following day!
        time.sleep(0.1)
    # DEBUG DELETE END

    time.sleep(100)
